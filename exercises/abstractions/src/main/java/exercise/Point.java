package exercise;

class Point {
    // BEGIN
    /*
    * makePoint() — принимает на вход координаты и возвращает точку.
    * getX() — принимает на вход точку и возвращает координату по оси x.
    * getY() — принимает на вход точку и возвращает координату по оси y.
    * pointToString() — принимает на вход точку и возвращает её строковое представление.
    *getQuadrant() — принимает на вход точку и возвращает номер квадранта, в котором эта точка расположена. Если точка не принадлежит ни одному квадранту (лежит на оси координат), то функция должна возвращать 0.
    * */
    public static int[] makePoint(int x, int y) {
        return new int[]{x, y};
    }

    public static int getX(int[] point){
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] point) {
        String result = "(" + getX(point) + ", " + getY(point) + ")";
        return result;
    }

    public static int getQuadrant(int[] point) {

        if      (getX(point) > 0 && getY(point) > 0) {
            return 1;
        }
        else if (getX(point) < 0 && getY(point) < 0) {
            return 3;
        }
        else if (getX(point) > 0 && getY(point) < 0) {
            return 4;
        }
        else if (getX(point) < 0 && getY(point) > 0) {
            return 2;
        }
        else {
            return 0;
        }
    }

    /*
    * публичный статический метод getSymmetricalPointByX().
    * принимает в качестве аргумента точку и
    * возвращает новую точку,
    * симметричную исходной относительно оси Х.
    * */
    public static int[] getSymmetricalPointByX(int[] point) {

        int[] result = new int[point.length];
        result[0] = point[0];
        result[1] = getY(point) * (-1);

        return result;
    }

    /*
    * публичный статический метод calculateDistance().
    * Метод принимает в качестве аргументов две точки и
    * возвращает расстояние между этими точками.
    * */
    public static double calculateDistance(int[] first, int[] second) {
        int x = getX(second) - getX(first);
        x *= x;

        int y = getY(second) - getY(first);
        y *=y;

        double distance = x + y;
        distance = Math.sqrt(distance);

        return distance;
    }

    public static void main(String[] args) {

        int[] point = makePoint(-3,-6);
        int[] symmetricalPoint = getSymmetricalPointByX(point);
        System.out.println(pointToString(symmetricalPoint));

        int[] point1 = makePoint(0,0);
        int[] point2 = makePoint(3,4);
        System.out.println(calculateDistance(point1, point2));
    }
    // END
}
