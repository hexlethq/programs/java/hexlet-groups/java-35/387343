package exercise;

import java.util.stream.Collectors;
import java.util.Arrays;

// BEGIN
class App {

    public static String getForwardedVariables(String config) {

        String[] lines = config.split("\n");

        return Arrays.stream(lines)
                .filter(line -> line.startsWith("environment="))
                .map(line -> line.replaceAll("environment=", ""))
                .filter(environment -> environment.contains("X_FORWARDED_"))
                .map(environment -> environment.replaceAll("\"", ""))
                .map(v -> v.split(","))
                .flatMap(Arrays::stream)
                .filter(pV -> pV.contains("X_FORWARDED_"))
                .map(variable -> variable.replaceAll("X_FORWARDED_", ""))
                .collect(Collectors.joining(","));
    }
}
//END
