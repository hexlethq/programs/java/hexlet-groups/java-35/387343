package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    /*Создайте публичный статический метод sort(),
    который сортирует переданный массив целых чисел,
    используя алгоритм пузырьковой сортировки.*/

    public static int[] sort(int[] array) {

        //  проверка на пустоту входного массива
        if (array.length == 0) {
            return array;
        }

        //  переделанная сортировка пузырьком
        for (int rL = array.length - 1; rL > -1 ; rL--) {
            for (int i = 0; i < rL; i++) {
                if (array[i] > array[i + 1]) {
                    int buffer = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buffer;
                }
            }
        }
        return array;
    }

    /*
    * Модифицируйте метод sort() таким образом,
    * чтобы он сортировал массив целых чисел,
    * используя алгоритм сортировки выбором.
    * */
    public static int[] selectSort(int[] array) {

        //  проверка на пустоту входного массива
        if (array.length == 0) {
            return array;
        }

        /*  leftLimit - предел внешнего цикла, сокращающийся слева (слева от него всё отсортировано).
        С ним сравнивается оставшийся диапазоном значений (кто меньше) для сортировки)*/
        for (int leftLimit = 0; leftLimit < array.length; leftLimit++) {

            //  minValueIndex - переменная, сохраняющая в себе индекс значения, которое окажется меньше leftLimit
            int minValueIndex = leftLimit;

            //  проход внутреннего цикла начинается на один элемент за leftLimit, чтобы он себя не сравнивал с собой же
            for (int i = leftLimit + 1; i < array.length; i++) {

                //  само сравнение leftLimit со значением текущей ячейки
                if (array[leftLimit] > array[i]) {

                    //  записываем найденный минимум
                    minValueIndex = i;

                    //  местами меняем
                    int buffer = array[minValueIndex];
                    array[minValueIndex] = array[leftLimit];
                    array[leftLimit] = buffer;
                }
            }
        }
        return array;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(sort(new int[]{9,8,7,6,5,4,3,2,1})));
        System.out.println(Arrays.toString(sort(new int[]{10, 1, 3})));
        System.out.println(Arrays.toString(sort(new int[]{0, 4, 0, 10, -3})));

        System.out.println(Arrays.toString(selectSort(new int[]{4,3,5,7,6,9,1})));
        System.out.println(Arrays.toString(selectSort(new int[]{9,8,7,6,5,4,3,2,1})));
        System.out.println(Arrays.toString(selectSort(new int[]{1,2,3,4,5,6,7})));
    }
    
    // END
}
