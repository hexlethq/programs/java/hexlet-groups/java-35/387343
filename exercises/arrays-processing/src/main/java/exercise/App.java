package exercise;

class App {

    // BEGIN

    /*
     * Создайте публичный статический метод getIndexOfMaxNegative(),
     * который принимает в качестве аргумента массив целых чисел и
     * возвращает индекс максимального отрицательного элемента массива.

     * * Если в массиве несколько таких одинаковых элементов,
     * нужно вернуть индекс первого из них.
     *
     * Если передан пустой массив
     * или в массиве нет отрицательных элементов,
     * метод должен вернуть -1.
     * */
    public static int getIndexOfMaxNegative(int[] input) {
        //  индекс искомого значения и само искомое значение
        int resultIndex = -1;
        int maxNegative = Integer.MIN_VALUE;

        //  цикл для анализа массива на пустоту и наличие негатива
        for (int i = 0; i < input.length; i++) {

            if (input[i] < 0 && input[i] > maxNegative) {
                resultIndex = i;
                maxNegative = input[i];
            }
        }
        return resultIndex;
    }

    /*
    публичный статический метод getElementsLessAverage().
    принимает на вход массив из целых чисел и
    возвращает новый массив,
    состоящий из элементов исходного массива.
    Каждый элемент нового массива должен быть не больше среднего арифметического всех элементов исходного массива.
    Если передан пустой массив, метод должен вернуть пустой массив.
    */
    public static int[] getElementsLessAverage(int[] input) {

        //  проверка на пустоту
        if (input.length == 0) {
            return input;
        }

        //  комплекс мер для вычисления среднего арифметического
        int sum = 0;
        for (int current : input) {
            sum += current;
        }
        int averageInput = sum / input.length;

        //  комплекс мер для вычисления количества элементов, значение которых ниже среднего арифметического
        int elementsLessAverage = 0;
        for (int current : input) {
            if (current <= averageInput) {
                elementsLessAverage++;
            }
        }

        //  создание результирующего массива
        int[] result = new int[elementsLessAverage];

        //  финальное прохождение исходного массива, чтобы заполнить результирующий массив
        for (int i = 0, j = 0; i < input.length; i++) {
            if (input[i] <= averageInput) {
                result[j] = input[i];
                j++;
            }
        }
        return result;
    }

    public static void main(String[] args) {

        int[] array1 = {5, 4, 34, 8, 11, -5, 1};
        System.out.println(getSumBeforeMinAndMax(array1));

        int[] array2 = {7, 1, 37, -5, 11, 8, 1};
        System.out.println(getSumBeforeMinAndMax(array2));
    }

    /*
    публичный статический метод getSumBeforeMinAndMax().
    принимает на вход массив из целых чисел и
    возвращает сумму элементов,
    которые расположены в массиве между минимальным и максимальным элементами массива
    (не включая сами минимальный и максимальный элементы).
    */
    public static int getSumBeforeMinAndMax(int[] input) {

        //  индексы и значения минимальных и максимальных элементов
        int min = Integer.MAX_VALUE;
        int indexMin = 0;
        int max = Integer.MIN_VALUE;
        int indexMax = 0;

        //  найдём минимальные и максимальные значения массива и их индексы
        for (int i = 0; i < input.length; i++) {

            if (input[i] > max) {
                max = input[i];
                indexMax = i;
            } else if (input[i] < min) {
                min = input[i];
                indexMin = i;
            }
        }
        int result = 0;
        int leftLimit = Integer.min(indexMin, indexMax);
        int rightLimit = Integer.max(indexMin, indexMax);

        for (int i = leftLimit + 1; i < rightLimit; i++) {
            result += input[i];
        }

        return result;
    }
    // END
}
