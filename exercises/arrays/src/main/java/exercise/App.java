package exercise;

import java.util.Arrays;

class App {

    // BEGIN

    /*публичный статический метод reverse(),
    который принимает в качестве аргумента массив целых чисел
    и возвращает новый массив целых чисел с элементами,
    расположенными в обратном порядке.

    Если передан пустой массив,
    метод должен вернуть пустой массив.*/
    public static int[] reverse(int[] input) {

        //  переделанная проверка пустоты входного массива
        if (input.length == 0) {
            return input;
        }

        int[] output = new int[input.length];
        /*
        * Цикл можно упростить,
        * если создавать всего одну переменную цикла,
        * которая будет принимать значения от 0 до длинны массива
        * и затем, в цикле,
        * просто использовать вот такую конструкцию output[i] = input[input.length - i - 1];.
        *
        * Такой подход избавит от необходимости использовать две переменных цикла
        * и делать проверку длинны массива в начале метода.
        * */

        for (int i = 0; i < input.length; i++) {
            output[i] = input[input.length - i - 1];
        }
        return output;
    }

    /*публичный статический метод mult().
    Метод принимает в качестве аргумента массив целых чисел
    и возвращает произведение всех элементов массива.*/
    public static int mult(int[] array) {

        int result = 1;

        for (int i = 0; i < array.length; i++) {
            result *= array[i];
        }
        return result;
    }

    /*  САМОСТАЯТЕЛЬНАЯ РАБОТА.
    Реализуйте публичный статический метод flattenMatrix().
    Метод принимает в качестве аргумента матрицу целых чисел (двумерный массив)
    и записывает все её элементы в одномерный массив.
    Если на вход передана пустая матрица,
    метод должен вернуть пустой массив. */
    public static int[] flattenMatrix(int[][] matrix) {

        //  высчитывание длины для одномерного массива
        int counter = 0;
        for (int i = 0; i < matrix.length; i++) {
            counter += matrix[i].length;
        }

        int[] result = new int[counter];

        //  заполнение одномерного массива значениями из двухмерного
        for (int i = 0, indexResult = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++, indexResult++) {
                result[indexResult] = matrix[i][j];
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[][] array = {{102, 22, 304}, {5678, 69012, 734567}};
        System.out.println("RESULT: " + Arrays.toString(flattenMatrix(array)));
    }
    // END
}
