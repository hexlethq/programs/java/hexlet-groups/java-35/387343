// BEGIN
package exercise;

import com.google.gson.Gson;

public class App {

    /*
    * публичный статический метод toJson(). Метод
    * принимает в качестве аргумента массив строк и
    * возвращает JSON представление массива в виде строки.
    *
    * Для преобразования массива в JSON
    * воспользуйтесь методами класса Gson из подключенной библиотеки.
    * */
    public static String toJson(String[] strings) {
        Gson gson = new Gson();
        String result = gson.toJson(strings);
        return result;
    }

    public static void main(String[] args) {

        System.out.println("Hello, World!");

//        String[] fruits = {"apple", "pear", "lemon"};
//        App.toJson(fruits); // => ["apple","pear","lemon"]
//        System.out.println(App.toJson(fruits));
    }
}
// END
