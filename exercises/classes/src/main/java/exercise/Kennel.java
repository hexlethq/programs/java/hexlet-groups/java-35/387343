package exercise;

import java.util.Arrays;


// BEGIN
/*
 * Создайте класс Kennel, представляющий питомник.
 *
 * Класс позволяет добавлять щенков в питомник и
 * вести учет их количества.
 *
 * Каждый щенок представлен массивом строк из двух элементов.
 * Первый элемент — имя щенка,
 * второй — его порода.*/
class Kennel {
    //    static String[][] puppies = new String[3][2];
    static String[][] animals = new String[5][2];
    static int countPuppy = 0;

    /*  WIN
     * addPuppy() —
     * принимает в качестве аргумента щенка и
     * добавляет его в питомник.*/
    public static void addPuppy(String[] puppy) {
        animals[countPuppy][0] = puppy[0];
        animals[countPuppy][1] = puppy[1];
        countPuppy++;
    }

    /*  WIN
     * getPuppyCount() —
     * возвращает общее количество щенков, находящихся на данный момент в питомнике.*/
    public static int getPuppyCount() {
        return countPuppy;
    }

    /*  WIN
     * addSomePuppies() —
     * принимает в качестве аргумента массив щенков и
     * добавляет их в питомник.*/
    public static void addSomePuppies(String[][] doggies) {
        for (String[] puppy : doggies) {
            addPuppy(puppy);
        }
    }

    /*  WIN
     * isContainPuppy() —
     * принимает в качестве аргумента имя щенка и
     * проверяет, есть ли в питомнике щенок с таким именем.
     *
     * Если щенок есть,
     * метод возвращает true,
     * в ином случае — false.*/
    public static boolean isContainPuppy(String name) {
        for (int i = 0; i < getPuppyCount(); i++) {
            if (name == animals[i][0])
                return true;
        }
        return false;
    }

    /*  EPIC WIN
     * getAllPuppies() —
     * возвращает всех щенков, имеющихся в питомнике,
     * в виде вложенного массива (смотрите пример вызова метода).
     *
     * Щенки в массиве должны располагаться в том порядке,
     * в каком они были добавлены в питомник.*/
    public static String[][] getAllPuppies() {
        String[][] allPuppies = new String[countPuppy][2];
        for (int i = 0; i < allPuppies.length; i++) {
            allPuppies[i] = animals[i];
        }
        return allPuppies;
    }

    /*  WIN
     * getNamesByBreed() —
     * принимает в качестве аргумента породу и
     * возвращает массив с именами щенков этой породы.*/
    public static String[] getNamesByBreed(String breed) {

        //  количество щенков указанной породы в питомнике
        int quantity = 0;
        for (int i = 0; i < animals.length; i++) {
            if (animals[i][1] == breed) {
                quantity++;
            }
        }

        //  массив на них
        String[] result = new String[quantity];

        //  заполнение массива
        for (int i = 0, j = 0; i < animals.length; i++) {
            if (animals[i][1] == breed) {
                result[j] = animals[i][0];
                j++;
            }
        }
        return result;
    }

    /*  WIN
     * resetKennel() —
     * очищает питомник, не оставляя в нем ни одного щенка.*/
    public static void resetKennel() {
        animals = new String[5][2];
        countPuppy = 0;
    }

    /*
     * публичный статический метод removePuppy(),
     * который позволяет выдавать щенка из питомника новому хозяину.
     * Метод принимает в качестве аргумента имя щенка.
     *
     * Если щенок с таким именем есть в питомнике,
     * метод удаляет его из питомника и возвращает true.
     *
     * Если щенок отсутствует, метод просто возвращает false.*/
    public static boolean removePuppy(String name) {

        //  если нужного щенка нет, завершаем
        if (!isContainPuppy(name)) {
            return false;
        }

        //  узнаем индекс щенка
        int indexPuppy = 0;
        for (int i = 0; i < animals.length; i++) {
            if (name == animals[i][0]) {
                indexPuppy = i;
            }
        }
        /*
         * сдвинем массив щенков, вычеркнув того, которого забирают.
         * Если он последний в массиве, то просто затрём последний элемент*/
        if (indexPuppy == countPuppy - 1) {
            animals[indexPuppy][0] = null;
            animals[indexPuppy][1] = null;

        } else {
            for (int i = indexPuppy; i < countPuppy - 1; i++) {
                animals[i] = animals[i + 1];
            }
            animals[getPuppyCount() - 1] = null;
        }
        countPuppy--;
        System.out.println("Держи, Герасим, своего щенка " + name);
        return true;
    }

    public static void main(String[] args) {
        String[] puppy1 = {"Rex", "boxer"};
        addPuppy(puppy1);
//        System.out.println(getPuppyCount());

        String[][] puppies2 = {
                {"Buddy", "chihuahua"},
                {"Toby", "chihuahua"},
        };
        addSomePuppies(puppies2);
//        System.out.println(getPuppyCount());

        System.out.println(isContainPuppy("Buddy"));
        System.out.println(isContainPuppy("Lassy"));

        String[][] pups = getAllPuppies();
        System.out.println("все щенки: " + Arrays.deepToString(pups));

        String[] names = getNamesByBreed("chihuahua");
        System.out.println(Arrays.toString(names));

//        System.out.println(removePuppy("Rex"));
        System.out.println(removePuppy("Buddy"));
//        System.out.println(removePuppy("Toby"));
        System.out.println(Arrays.deepToString(animals));
    }
}
// END
