package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int first, int second, int third) {
        if (    (first + second) > third &&
                (first + third) > second &&
                (second + third) > first    ) {

            if (first != second && second != third && third != first) {
                return "Разносторонний";
            }
            else if (first == second && second == third) {
                return "Равносторонний";
            }
            else {
                return "Равнобедренный";
            }
        }
        return "Треугольник не существует";
    }

//    Самостоятельная работа. Ниже метод main, чтобы можно было сразу проверить этот метод
    public static int getFinalGrade(int exam, int project) {
        if (exam > 90 || project > 10) {
            return 100;
        }
        else if (exam > 75 && project >= 5) {
            return 90;
        }
        else if (exam > 50 && project >= 2) {
            return 75;
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(getFinalGrade(100, 12));
        System.out.println(getFinalGrade(99, 0));
        System.out.println(getFinalGrade(10, 15));
        System.out.println(getFinalGrade(85, 5));
        System.out.println(getFinalGrade(55, 3));
        System.out.println(getFinalGrade(55, 0));
    }
    // END
}
