package exercise;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

// BEGIN
public class Sorter {

    public static void main(String[] args) {
        List<Map<String, String>> users = List.of(
                Map.of("name", "Vladimir Nikolaev", "birthday", "1990-12-27", "gender", "male"),
                Map.of("name", "Andrey Petrov", "birthday", "1989-11-23", "gender", "male"),
                Map.of("name", "Anna Sidorova", "birthday", "1996-09-09", "gender", "female"),
                Map.of("name", "John Smith", "birthday", "1989-03-11", "gender", "male"),
                Map.of("name", "Vanessa Vulf", "birthday", "1985-11-16", "gender", "female"),
                Map.of("name", "Alice Lucas", "birthday", "1986-01-01", "gender", "female"),
                Map.of("name", "Elsa Oscar", "birthday", "1970-03-10", "gender", "female")
        );
        List<String> men = Sorter.takeOldestMans(users);
        System.out.println("main " + men);
    }

    public static List<String> takeOldestMans(List<Map<String, String>> users) {

        //  создание списка мапов, где содержатся только мужчины
        List<Map<String, String>> men = users.stream()
                .filter(user -> user.containsValue("male"))
                .collect(Collectors.toList());

        //  подготовка к распознаванию дат
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        //  отдельные списки дат и имён
        List<Date> dates = new ArrayList<>();
        List<String> names = new ArrayList<>();

        //  занесение данных в списки имён и дат из списка мапов, где содержатся только мужчины
        for (Map<String, String> man : men) {
            for (Map.Entry currentMan : man.entrySet()) {
                if (!currentMan.getValue().equals("male")) {
                    String current = String.valueOf(currentMan.getValue());
                    if (current.toCharArray()[0] == '1') {
                        try {
                            dates.add(format.parse(current));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        names.add(current);
                    }
                }
            }
        }

        //  соединение двух списков в одну мапу, где ключ - дата, а значение - имя
        Map<Date, String> unsortedMap = new LinkedHashMap<>();
        for (int i = 0; i < dates.size(); i++) {
            unsortedMap.put(dates.get(i), names.get(i));
        }

        //  перегон мапы в отсортированную мапу
        Map<Date, String> sortedMap = new TreeMap<>(unsortedMap);

        //  вынимание имён в правильном порядке
        List<String> resultNames = new LinkedList<>();
        for (Map.Entry<Date, String> currentSorted : sortedMap.entrySet()) {
            resultNames.add(currentSorted.getValue());
        }
        return resultNames;
    }
}
// END
