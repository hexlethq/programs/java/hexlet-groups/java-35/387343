package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// BEGIN
class App {

    public static boolean scrabble(String symbols, String word) {

        String[] lettersOfWord = word.split("");
        List<String> desired = new ArrayList<>(Arrays.asList(lettersOfWord));

        String[] letters = symbols.split("");
        List<String> kit = new ArrayList<>(Arrays.asList(letters));

        for (String currentSymbolOfTheWord : desired) {
            String currentInString = currentSymbolOfTheWord.toLowerCase();

            if (!kit.remove(currentInString)) {
                return false;
            }
        }
        return true;
    }
}
//END
