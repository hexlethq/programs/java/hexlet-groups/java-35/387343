package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String string) {

        String workingString = string.trim();
        String result = "";

        for (int i = 0; i < workingString.length(); i++) {
            if (i == 0) {
                result += workingString.charAt(i);
            }
            else if (workingString.charAt(i) == ' ' && workingString.charAt(i + 1) != ' ') {
                result += workingString.charAt(i + 1);
            }
        }
        return result.toUpperCase();
    }
    // END
}
