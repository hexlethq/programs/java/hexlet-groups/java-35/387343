package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static void main(String[] args) {
        Map wordsCount = getWordCount("java is the best programming language java");
        System.out.println(wordsCount);

        Map wordsCount2 = getWordCount("word text dog apple word apple word");
        System.out.println(wordsCount2);
    }

    public static String toString(Map<String, Integer> map) {
        if (map.isEmpty()) {
            return "{}";
        }
        String result = "{";
        for (Map.Entry<String, Integer> thePair : map.entrySet()) {
            result += "\n  " + thePair.getKey() + ": " + thePair.getValue();
        }
        result += "\n}";
        return result;
    }

    public static Map<String, Integer> getWordCount(String string) {
        if (string.isEmpty()) {
            return new HashMap<>();
        }
        String[] splitedWords = string.split(" ");
        Map<String, Integer> result = new HashMap<>();
        for (String word : splitedWords) {
            result.put(word, result.getOrDefault(word, 0) + 1);
        }
        return result;
    }

    /*public static Map<String, Integer> getWordCount(String string) {
        if (string.isEmpty()) {
            return new HashMap<>();
        }

        String[] splitedWords = string.split(" ");
        Map<String, Integer> result = new HashMap<>();

        for (String word : splitedWords) {
            if (!result.containsKey(word)) {
                result.put(word, 1);
            } else {
                for (Map.Entry<String, Integer> thePair : result.entrySet()) {
                    if (thePair.getKey().equals(word)) {
                        int oldValue = thePair.getValue();
                        thePair.setValue(oldValue + 1);
                    }
                }
            }
        }
        return result;
    }*/
}
//END
