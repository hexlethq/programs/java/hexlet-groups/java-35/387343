package exercise;

class Converter {
    // BEGIN
    public static int convert(int numberForConvertation, String direction) {
        int result = 0;
        if (direction == "b") {
            result = numberForConvertation * 1024;
        }
        else if (direction == "Kb") {
            result = numberForConvertation / 1024;
        }
        return result;
    }

    /*  когда в задании появляется инструкция, что нужно реализовать метод main,
    * совсем неочевидно, что речь про тот самый main(), который точкой входа является.
    * Пришлось чуть понервничать. Совсем новички могут растеряться.     */
    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    // END
}
