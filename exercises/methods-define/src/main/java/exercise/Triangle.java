package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int firstK, int secondK, double angle) {
        double radAngle = angle * Math.PI / 180;
        return  ((firstK * secondK) / 2) * Math.sin(radAngle);
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
