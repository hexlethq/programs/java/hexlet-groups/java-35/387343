package exercise;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import static java.time.temporal.ChronoUnit.DAYS;

class App {
    // BEGIN
    /*публичный статический метод buildList(), который принимает на вход массив строк и
     * преобразует его в маркированный html список.
     *
     * Если передан пустой массив, метод должен возвращать пустую строку.
     *
     * Для сборки строки используйте класс StringBuilder.
     * Не забудьте про отступы и переносы строк.*/
    public static String buildList(String[] input) {
        if (input.length == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder("<ul>\n");
        for (int i = 0; i < input.length; i++) {
            builder.append("  <li>" + input[i] + "</li>\n");
            if (i == input.length - 1) {
                builder.append("</ul>");
            }
        }
        return builder.toString();
    }

    /*
     * публичный статический метод getUsersByYear(), который
     * принимает в качестве аргументов
     * массив пользователей
     * и год.
     *
     * Каждый пользователь представлен массивом строк,
     * в котором
     * первый элемент — это имя пользователя, а
     * второй — дата рождения.
     *
     * Метод должен возвращать маркированный html список
     * с именами пользователей,
     * которые родились в указанный год.
     * */
    public static String getUsersByYear(String[][] users, int year) {

        //  год перевожу в строчный вид
        String stringYear = String.valueOf(year);

        //  счётчик совпадений по году
        int counter = 0;

        //  цикл поиска совпадений по году
        for (int i = 0; i < users.length; i++) {
            if (users[i][1].contains(stringYear)) {
                counter++;
            }
        }

        //  список имён и запись их при повторном прохождении массива
        String[] names = new String[counter];
        for (int i = 0, j = 0; i < users.length; i++) {
            if (users[i][1].contains(stringYear)) {
                names[j] = users[i][0];
                j++;
            }
        }
        //  вызов созданного метода и возврат значения
        String result = buildList(names);
        return result;
    }

    // END

    /*
     * публичный статический метод getYoungestUser(), который
     * принимает в качестве аргументов
     * массив пользователей и
     * дату в виде строки формата "3 Sep 1975".
     *
     * Метод должен
     * возвращать имя самого младшего из пользователей,
     * которые родились до указанной даты.
     *
     * В примере ниже только четыре пользователя родились до 11 июля 1989 года.
     * Это пользователи с именами
     * "John Smith", "Vanessa Vulf", "Alice Lucas" и "Elsa Oscar".
     * Самый младший из них — пользователь "John Smith",
     * так как он родился позже остальных в этой группе.
     * Если никто из пользователей не родился до указанной даты,
     * метод должен вернуть пустую строку
     * */
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN

        //  перевожу строковую дату в настоящую
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy", Locale.ENGLISH);
        LocalDate theDate = LocalDate.parse(date, formatter);
        System.out.println(theDate);

        //  распилим входной массив на 2 части: массив строковых дат и массив имён
        String[] names = new String[users.length];
        String[] stringDates = new String[users.length];
        for (int i = 0; i < users.length; i++) {
            names[i] = users[i][0];
            stringDates[i] = users[i][1];
        }

        //  массив строковых дат переведём в массив настоящих дат
        LocalDate[] listDate = new LocalDate[stringDates.length];
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
        for (int i = 0; i < stringDates.length; i++) {
            listDate[i] = LocalDate.parse(stringDates[i], formatter);
        }

        /*
        * разница между датами в днях должна быть отрицательной (зависит от условия.
        * но для других вариантов писать не стал (:).
        * Среди отфильтрованных потенциальных вариантов ищу минимальную разницу по модулю
        * */
        int minIndex = Integer.MIN_VALUE;
        int minDifference = Integer.MAX_VALUE;
        for (int i = 0; i < listDate.length; i++) {
            if (DAYS.between(theDate, listDate[i]) < 0) {
                if (minDifference > Math.abs(DAYS.between(theDate, listDate[i]))) {
                    minDifference = (int) DAYS.between(theDate, listDate[i]);
                    minIndex = i;
                }
            }
        }
        return names[minIndex];
        // END
    }

    public static void main(String[] args) throws Exception {
        String[][] users = {
                {"Andrey Petrov", "1990-11-23"},
                {"Aleksey Ivanov", "2000-02-15"},
                {"Anna Sidorova", "1996-09-09"},
                {"John Smith", "1989-03-11"},
                {"Vanessa Vulf", "1985-11-16"},
                {"Vladimir Nikolaev", "1990-12-27"},
                {"Alice Lucas", "1986-01-01"},
                {"Elsa Oscar", "1989-03-10"},
        };
        System.out.println(getYoungestUser(users, "11 Jul 1989"));
    }
}
