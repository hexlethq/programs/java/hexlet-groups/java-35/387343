// BEGIN
package exercise;
import exercise.geometry.*;

import java.util.Arrays;
//import exercise.geometry.Point;
//import exercise.geometry.Segment;

class App {

    //  getMidpointOfSegment() — принимает в качестве аргумента отрезок и возвращает точку середины отрезка.
    public static double[] getMidpointOfSegment(double[][] segment) {

        double x1 = segment[0][0];
        double y1 = segment[0][1];

        double x2 = segment[1][0];
        double y2 = segment[1][1];

        double xMidPoint = (x1 + x2) / 2;
        double yMidPoint = (y1 + y2) / 2;

        double[] midPoint = {xMidPoint, yMidPoint};

        return midPoint;
    }

    /*
    * reverse() —
    * принимает в качестве аргумента отрезок и
    * возвращает новый отрезок с точками,
    * добавленными в обратном порядке (begin меняется местами с end).
    *
    * Точки в результирующем отрезке
    * должны быть копиями (по значению) соответствующих точек исходного отрезка.
    * То есть они не должны быть ссылкой на одну и ту же точку,
    * так как это разные точки (пускай и с одинаковыми координатами).
    * */
    public static double[][] reverse(double[][] segment) {

        double[][] reverseSegment = new double[2][2];

        reverseSegment[1][0] = segment[0][0];
        reverseSegment[1][1] = segment[0][1];

        reverseSegment[0][0] = segment[1][0];
        reverseSegment[0][1] = segment[1][1];

        return reverseSegment;
    }

    /*
    * публичный статический метод isBelongToOneQuadrant().
    * Метод принимает на вход отрезок и
    * определяет, лежит ли весь отрезок целиком в одном квадранте.
    *
    * Если начало и конец отрезка лежат в одном квадранте,
    * метод возвращает true,
    * иначе — false.
    *
    * Если хотя бы одна из точек лежит на оси координат,
    * то считается, что отрезок не находится целиком в одном квадранте. false
    * */
    public static boolean isBelongToOneQuadrant(double[][] segment) {
        //  флажки на наличие положительных и отрицательныхзначений
        boolean hasPositive = false;
        boolean hasNegative = false;

        //  пробегаем по массиву
        for (int i = 0; i < segment.length; i++) {
            for(int k = 0; k < segment[i].length; k++) {
                //  поднимаем подходящие флажки
                if (segment[i][k] > 0) {
                    hasPositive = true;
                } else if (segment[i][k] < 0){
                    hasNegative = true;
                } else {
                    //  этот случай означает, что точка лежит на оси координат, а значит сразу false
                    return false;
                }
            }
        }
            //  если ТОЛЬКО положительный или только отрицательный, то true
        if ((hasPositive && !hasNegative) || (hasNegative && !hasPositive)) {
            return true;
            //  если и положительный и отрицательный, то false
        } else {
            return false;
        }
    }

    public static void main(String[] args) {

        double[] point1 = Point.makePoint(2, 3);
        double[] point2 = Point.makePoint(4, 5);

        double[][] segment = Segment.makeSegment(point1, point2);

        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);

        System.out.println(Arrays.toString(beginPoint)); // => [2, 3]
        System.out.println(Arrays.toString(endPoint)); // => [4, 5]


        double[] point3 = Point.makePoint(3, 4);
        double[] point4 = Point.makePoint(6, 7);
        double[][] segment2 = Segment.makeSegment(point3, point4);

        double[] midPoint = App.getMidpointOfSegment(segment2);
        System.out.println(Arrays.toString(midPoint)); // => [4.5, 5.5]

        double[][] reversedSegment = App.reverse(segment2);
        double[] beginPoint2 = Segment.getBeginPoint(reversedSegment);
        double[] endPoint2 = Segment.getEndPoint(reversedSegment);

        System.out.println(Arrays.toString(beginPoint2)); // => [6, 7]
        System.out.println(Arrays.toString(endPoint2)); // => [3, 4]



        /*System.out.println();
        double[][] segment10 = Segment.makeSegment(Point.makePoint(1, 4), Point.makePoint(5, 8));
        System.out.println(App.isBelongToOneQuadrant(segment10));   // true
        double[][] segment20 = Segment.makeSegment(Point.makePoint(1, 4), Point.makePoint(-2, 8));
        System.out.println(App.isBelongToOneQuadrant(segment20));   // false
        double[][] segment30 = Segment.makeSegment(Point.makePoint(1, 4), Point.makePoint(0, 0));
        System.out.println(App.isBelongToOneQuadrant(segment30));   // false
        */
    }
}
// END
