// BEGIN
package exercise.geometry;

public class Segment {

//    makeSegment() — принимает на вход две точки и возвращает отрезок.
    public static double[][] makeSegment(double[] first, double[] second) {

        double[][] result = new double[2][2];

        result[0][0] = first[0];
        result[0][1] = first[1];

        result[1][0] = second[0];
        result[1][1] = second[1];

        return result;
    }

//    getBeginPoint() — принимает на вход отрезок и возвращает точку начала отрезка.
    public static double[] getBeginPoint(double[][] segment) {

        double[] beginPoint = new double[2];
        beginPoint[0] = segment[0][0];
        beginPoint[1] = segment[0][1];

        return beginPoint;
    }

//    getEndPoint() — Принимает на вход отрезок и возвращает точку конца отрезка.
public static double[] getEndPoint(double[][] segment) {

    double[] endPoint = new double[2];
    endPoint[0] = segment[1][0];
    endPoint[1] = segment[1][1];

    return endPoint;
}
}
// END
