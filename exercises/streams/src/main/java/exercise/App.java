package exercise;

import java.util.List;

// BEGIN
public class App {

    public static int getCountOfFreeEmails(List<String> emails) {
        List<String> freeDomainsNew = List.of("gmail.com", "yandex.ru", "hotmail.com");

        return (int) emails.stream()
                .map(email -> email.substring(email.lastIndexOf("@") + 1))
                .filter(freeDomainsNew::contains)
                .count();
    }
}
// END
